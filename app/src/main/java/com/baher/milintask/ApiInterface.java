package com.baher.milintask;

import com.baher.milintask.models.Pexels;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.Query;

public interface ApiInterface {

    @Headers("Authorization:563492ad6f91700001000001e29b4f665bc64043b514c56b99f897a9")
    @GET("search")
    Call<Pexels> GetPhotos(
            @Query("query") String query ,
            @Query("per_page") int per_page ,
            @Query("page") int page
    );
}
