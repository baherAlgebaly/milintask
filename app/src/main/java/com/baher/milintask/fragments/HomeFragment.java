package com.baher.milintask.fragments;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.baher.milintask.ApiInterface;
import com.baher.milintask.R;
import com.baher.milintask.models.Pexels;
import com.baher.milintask.adapters.ImagesAdapter;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link HomeFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class HomeFragment extends Fragment implements
        View.OnKeyListener {

    // TODO: Rename parameter arguments, choose names that match
    // the fragment initialization parameters, e.g. ARG_ITEM_NUMBER
    private static final String ARG_PARAM1 = "param1";
    private static final String ARG_PARAM2 = "param2";



    // TODO: Rename and change types of parameters
    private String mParam1;
    private String mParam2;


    public EditText edtSearch ;
    public RecyclerView recPhotos ;
    public Retrofit retrofit ;
    public ApiInterface apiInterface ;
    private ImagesAdapter imagesAdapter ;

    public HomeFragment() {
        // Required empty public constructor
    }


    // TODO: Rename and change types and number of parameters
    public static HomeFragment newInstance(String param1, String param2) {
        HomeFragment fragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1, param1);
        args.putString(ARG_PARAM2, param2);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {
            mParam1 = getArguments().getString(ARG_PARAM1);
            mParam2 = getArguments().getString(ARG_PARAM2);
        }

        retrofit = new Retrofit.Builder()
                .baseUrl("https://api.pexels.com/v1/")
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        apiInterface = retrofit.create(ApiInterface.class);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_home, container, false);

        edtSearch = (EditText)view.findViewById(R.id.edtSearch);
        recPhotos = (RecyclerView)view.findViewById(R.id.recPhotos);
        recPhotos.setLayoutManager(new GridLayoutManager(getContext(),2));

        fetchData("happy",1);

        edtSearch.setOnKeyListener(this);
        return view ;
    }

    public void fetchData(String query , int page){

        Call<Pexels> call = apiInterface.GetPhotos(query,20,page);

        call.enqueue(new Callback<Pexels>() {
            @Override
            public void onResponse(Call<Pexels> call, Response<Pexels> response) {

                Pexels pexels = response.body();
                imagesAdapter = new ImagesAdapter(getContext(),pexels.getPhotos());

                recPhotos.setAdapter(imagesAdapter);

            }

            @Override
            public void onFailure(Call<Pexels> call, Throwable t) {

            }
        });



    }

    @Override
    public boolean onKey(View v, int keyCode, KeyEvent event) {

        if (event.getAction() == KeyEvent.ACTION_DOWN && keyCode == KeyEvent.KEYCODE_ENTER) {
           fetchData(edtSearch.getText().toString(),1);
            return true;
        }

        return false;
    }
}