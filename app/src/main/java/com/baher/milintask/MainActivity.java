package com.baher.milintask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import android.app.DownloadManager;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.os.Bundle;
import android.widget.EditText;

import com.baher.milintask.fragments.CameraFragment;
import com.baher.milintask.fragments.HomeFragment;
import com.baher.milintask.models.Pexels;
import com.ismaeldivita.chipnavigation.ChipNavigationBar;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class MainActivity extends AppCompatActivity {

   public FragmentTransaction ft ;
   public static ChipNavigationBar chipNavBar ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        chipNavBar = (ChipNavigationBar)findViewById(R.id.chipNavBar);


        ft = getSupportFragmentManager().beginTransaction();
        ft.replace(R.id.container,new HomeFragment());
       // ft.addToBackStack("home");
        ft.commit();

        chipNavBar.setItemSelected(R.id.home,true);

        chipNavBar.setOnItemSelectedListener(new ChipNavigationBar.OnItemSelectedListener() {
            @Override
            public void onItemSelected(int id) {

                switch (id){
                    case R.id.home :
                        ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.container,new HomeFragment());
                       // ft.addToBackStack("home");
                        ft.commit();
                        break;

                    case R.id.camera :
                        ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.container,new CameraFragment());
                       // ft.addToBackStack("camera");
                        ft.commit();
                        break;

                    case R.id.device :
                        ft = getSupportFragmentManager().beginTransaction();
                        ft.replace(R.id.container,new DeviceFragment());
                       // ft.addToBackStack("device");
                        ft.commit();
                        break;
                }


            }
        });



    }




}