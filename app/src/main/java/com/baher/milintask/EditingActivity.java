package com.baher.milintask;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.annotation.SuppressLint;
import android.app.ActionBar;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.baher.milintask.adapters.FiltersAdapter;
import com.skydoves.transformationlayout.TransformationAppCompatActivity;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import eightbitlab.com.blurview.BlurView;
import eightbitlab.com.blurview.RenderScriptBlur;

public class EditingActivity extends TransformationAppCompatActivity implements View.OnTouchListener {

    public Bundle bundle ;
    boolean fromCamera ;
    public TextView txtImageName , txtGrapher ;
    public static ImageView imgMain ;
    public BlurView blurView ;
    public RecyclerView recFilters ;

    Bitmap srcBitMap ;

    public static Bitmap toModifyBitMap ;
    public List<Bitmap> filters ;

    public static PhotoFilter photoFilter ;
    public FiltersAdapter filtersAdapter ;

    public ImageView imgWrite ;
    TextView textView;
    ViewGroup root;
    private int xDelta;
    private int yDelta;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editing);

        bundle = getIntent().getExtras();

        txtImageName = (TextView) findViewById(R.id.txtImageName);
        txtGrapher = (TextView) findViewById(R.id.txtGrapher);
        imgMain = (ImageView) findViewById(R.id.imgMain);
        blurView = (BlurView) findViewById(R.id.blurView);
        recFilters = (RecyclerView) findViewById(R.id.recFilters);

        imgWrite =(ImageView)findViewById(R.id.imgWrite);
        root = (ViewGroup)findViewById(R.id.root);
        textView = (TextView)findViewById(R.id.dragText);

        if (!bundle.getBoolean("fromCamera",false)){
            String srcUrl = bundle.getString("bitmap");
            Picasso.get().load(srcUrl).into(imgMain);
        }else if(bundle.getBoolean("fromCamera",false)){
            byte [] bytes = bundle.getByteArray("byte") ;
            Bitmap bitmap = BitmapFactory.decodeByteArray(bytes,0,bytes.length);
            imgMain.setImageBitmap(bitmap);
        }




        recFilters.setLayoutManager(new LinearLayoutManager(EditingActivity.this, LinearLayoutManager.HORIZONTAL, false));


        float radius = 14f;

        View decorView = getWindow().getDecorView();
        ViewGroup rootView = (ViewGroup) decorView.findViewById(android.R.id.content);
        Drawable windowBackground = decorView.getBackground();

        blurView.setupWith(rootView)
                .setFrameClearDrawable(windowBackground)
                .setBlurAlgorithm(new RenderScriptBlur(this))
                .setBlurRadius(radius)
                .setBlurAutoUpdate(true)
                .setHasFixedTransformationMatrix(true);

        filters = new ArrayList<>();
        photoFilter = new PhotoFilter();

        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                 srcBitMap = ((BitmapDrawable)imgMain.getDrawable()).getBitmap();

                filters.add(photoFilter.eleven(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.twelve(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.thirteen(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.fourteen(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.fifteen(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.sixteen(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.two(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.three(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.four(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.five(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.six(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.seven(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.eight(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.nine(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.ten(getApplicationContext(),srcBitMap));
                filters.add(photoFilter.one(getApplicationContext(),srcBitMap));


                filtersAdapter = new FiltersAdapter(EditingActivity.this,filters);

                recFilters.setAdapter(filtersAdapter);

                toModifyBitMap = ((BitmapDrawable)imgMain.getDrawable()).getBitmap();
            }
        }, 800);




        txtImageName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                imgMain.setImageBitmap(drawTextToBitmap(EditingActivity.this,srcBitMap,"Test"));
            }
        });

        imgWrite.setOnClickListener(new View.OnClickListener() {
            @SuppressLint("ClickableViewAccessibility")
            @Override
            public void onClick(View v) {
                textView.setAlpha(1);
                textView.setElevation(22);


                RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.WRAP_CONTENT, ViewGroup.LayoutParams.WRAP_CONTENT );
                layoutParams.leftMargin = 50;
                layoutParams.topMargin = 50;
                layoutParams.bottomMargin = -250;
                layoutParams.rightMargin = -250;
                textView.setLayoutParams(layoutParams);

                textView.setOnTouchListener(EditingActivity.this::onTouch);
//                root.addView(textView);
            }
        });

    }

    public static void filterApplier (Context context, int position){

        switch (position){
            case 0 :
                imgMain.setImageBitmap(photoFilter.eleven(context,toModifyBitMap));
                break;

            case 1:
                imgMain.setImageBitmap(photoFilter.twelve(context,toModifyBitMap));
                break;

            case 2:
                imgMain.setImageBitmap(photoFilter.thirteen(context,toModifyBitMap));
                break;
            case 3:
                imgMain.setImageBitmap(photoFilter.fourteen(context,toModifyBitMap));
                break;
            case 4:
                imgMain.setImageBitmap(photoFilter.fifteen(context,toModifyBitMap));
                break;

            case 5:
                imgMain.setImageBitmap(photoFilter.sixteen(context,toModifyBitMap));
                break;
            case 6:
                imgMain.setImageBitmap(photoFilter.two(context,toModifyBitMap));
                break;
            case 7:
                imgMain.setImageBitmap(photoFilter.three(context,toModifyBitMap));
                break;
            case 8:
                imgMain.setImageBitmap(photoFilter.four(context,toModifyBitMap));
                break;
            case 9:
                imgMain.setImageBitmap(photoFilter.five(context,toModifyBitMap));
                break;
            case 10:
                imgMain.setImageBitmap(photoFilter.six(context,toModifyBitMap));
                break;
            case 11:
                imgMain.setImageBitmap(photoFilter.seven(context,toModifyBitMap));
                break;
            case 12:
                imgMain.setImageBitmap(photoFilter.eight(context,toModifyBitMap));
                break;
            case 13:
                imgMain.setImageBitmap(photoFilter.nine(context,toModifyBitMap));
                break;
            case 14:
                imgMain.setImageBitmap(photoFilter.ten(context,toModifyBitMap));
                break;
            case 15:
                imgMain.setImageBitmap(photoFilter.one(context,toModifyBitMap));
                break;
        }

    }

    public Bitmap drawTextToBitmap(Context context, Bitmap toModifyBitMap, String gText) {

        Resources resources = context.getResources();
        float scale = resources.getDisplayMetrics().density;

        android.graphics.Bitmap.Config bitmapConfig =
                toModifyBitMap.getConfig();
        // set default bitmap config if none
        if(bitmapConfig == null) {
            bitmapConfig = android.graphics.Bitmap.Config.ARGB_8888;
        }
        // resource bitmaps are imutable,
        // so we need to convert it to mutable one
        toModifyBitMap = toModifyBitMap.copy(bitmapConfig, true);

        Canvas canvas = new Canvas(toModifyBitMap);
        // new antialised Paint
        Paint paint = new Paint(Paint.ANTI_ALIAS_FLAG);
        // text color - #3D3D3D
        paint.setColor(Color.rgb(255, 255, 255));
        // text size in pixels
        paint.setTextSize((int) (24 * scale));
        // text shadow
        paint.setShadowLayer(1f, 0f, 1f, Color.WHITE);

        // draw text to the Canvas center
        Rect bounds = new Rect();
        paint.getTextBounds(gText, 0, gText.length(), bounds);
        float x = textView.getX(); //(toModifyBitMap.getWidth() - bounds.width())/2;
        float y = textView.getY(); //(toModifyBitMap.getHeight() + bounds.height())/2;

        canvas.drawText(gText, x, y, paint);

        return toModifyBitMap;
    }


    @Override
    public boolean onTouch(View view, MotionEvent event) {
        final int X = (int) event.getRawX();
        final int Y = (int) event.getRawY();
        switch (event.getAction() & MotionEvent.ACTION_MASK) {
            case MotionEvent.ACTION_DOWN:
                RelativeLayout.LayoutParams lParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                xDelta = X - lParams.leftMargin;
                yDelta = Y - lParams.topMargin;
                break;
            case MotionEvent.ACTION_UP:
                break;
            case MotionEvent.ACTION_POINTER_DOWN:
                break;
            case MotionEvent.ACTION_POINTER_UP:
                break;
            case MotionEvent.ACTION_MOVE:
                RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams) view.getLayoutParams();
                layoutParams.leftMargin = X - xDelta;
                layoutParams.topMargin = Y - yDelta;
                layoutParams.rightMargin = -250;
                layoutParams.bottomMargin = -250;
                view.setLayoutParams(layoutParams);
                break;
        }
        root.invalidate();
        return true;
    }
}