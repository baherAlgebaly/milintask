package com.baher.milintask.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.baher.milintask.EditingActivity;
import com.baher.milintask.R;
import com.baher.milintask.models.Photo;
import com.baher.milintask.models.Src;
import com.skydoves.transformationlayout.TransformationCompat;
import com.skydoves.transformationlayout.TransformationLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

public class FiltersAdapter extends RecyclerView.Adapter<FiltersAdapter.MyView>{

    Context context ;
    List<Bitmap> filters ;

    public int selectedPosition = 0 ;


    public class MyView
            extends RecyclerView.ViewHolder {

        ImageView imageView ;

        public MyView(View view)
        {
            super(view);


            imageView = (ImageView)view.findViewById(R.id.imageView2);

        }
    }


    public FiltersAdapter(Context context , List <Bitmap> filters )
    {
        this.context = context ;
        this.filters = filters ;

    }


    @Override
    public FiltersAdapter.MyView onCreateViewHolder(ViewGroup parent,
                                                   int viewType)
    {

        View itemView
                = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_filter,
                        parent,
                        false);

        return new FiltersAdapter.MyView(itemView);
    }

    // Override onBindViewHolder which deals
    // with the setting of different data
    // and methods related to clicks on
    // particular items of the RecyclerView.

    @Override
    public void onBindViewHolder(@NonNull FiltersAdapter.MyView holder, int position) {
        // Set the text of each item of
        // Recycler view with the list items
        holder.imageView.setImageBitmap(filters.get(position));

        holder.imageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                EditingActivity.filterApplier(context,position);
            }
        });



    }




    // Override getItemCount which Returns
    // the length of the RecyclerView.
    @Override
    public int getItemCount()
    {
        return filters.size() ;
    }

    public int getSelectedPosition (){
        return selectedPosition ;
    }
}
