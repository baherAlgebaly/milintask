package com.baher.milintask.adapters;

import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.baher.milintask.EditingActivity;
import com.baher.milintask.R;
import com.baher.milintask.models.Photo;
import com.baher.milintask.models.Src;
import com.skydoves.transformationlayout.TransformationCompat;
import com.skydoves.transformationlayout.TransformationLayout;
import com.squareup.picasso.Picasso;

import java.util.List;

public class ImagesAdapter extends RecyclerView.Adapter<ImagesAdapter.MyView>{

    Context context ;
    List<Photo> photos ;


    public class MyView
            extends RecyclerView.ViewHolder {

        // Text View
        TextView txtPhotographer ;
        ImageView imageView ;
        FrameLayout frameLayout ;
        TransformationLayout transformationLayout ;



        // parameterised constructor for View Holder class
        // which takes the view as a parameter
        public MyView(View view)
        {
            super(view);

            // initialise TextView with id
            txtPhotographer = (TextView)view
                    .findViewById(R.id.txtPhotographer);
            imageView = (ImageView)view.findViewById(R.id.imageView);
            // doctors = new ArrayList<>();
            transformationLayout = (TransformationLayout)view.findViewById(R.id.transformationLayout);

        }
    }

    // Constructor for adapter class
    // which takes a list of String type
    public ImagesAdapter(Context context , List <Photo> photos )
    {
        this.context = context ;
        this.photos = photos ;

    }

    // Override onCreateViewHolder which deals
    // with the inflation of the card layout
    // as an item for the RecyclerView.
    @Override
    public ImagesAdapter.MyView onCreateViewHolder(ViewGroup parent,
                                                int viewType)
    {

        // Inflate item.xml using LayoutInflator
        View itemView
                = LayoutInflater
                .from(parent.getContext())
                .inflate(R.layout.item_image,
                        parent,
                        false);

        // return itemView
        return new ImagesAdapter.MyView(itemView);
    }

    // Override onBindViewHolder which deals
    // with the setting of different data
    // and methods related to clicks on
    // particular items of the RecyclerView.

    @Override
    public void onBindViewHolder(@NonNull MyView holder, int position) {
        // Set the text of each item of
        // Recycler view with the list items


        holder.txtPhotographer.setText(photos.get(position).getPhotographer());
        Src src = photos.get(position).getSrc();

        String portraitSrcUrl = src.getPortrait();
        Picasso.get().load(portraitSrcUrl).into(holder.imageView);

        holder.transformationLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
             //   Bitmap bitmap  = ((BitmapDrawable) holder.imageView.getDrawable()).getBitmap();

                Intent intent = new Intent(context, EditingActivity.class);
                intent.putExtra("bitmap",portraitSrcUrl);
                TransformationCompat.startActivity(holder.transformationLayout, intent);
            }
        });




    }




    // Override getItemCount which Returns
    // the length of the RecyclerView.
    @Override
    public int getItemCount()
    {
        return photos.size() ;
    }
}

